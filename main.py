# https://keras.io/examples/timeseries/timeseries_weather_forecasting/

import enum
from types import DynamicClassAttribute
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras

import os

from tensorflow.python.ops.array_ops import sequence_mask

# --- Read CSV File ---
csv_path = "jena_climate_2009_2016_mod.csv"
df = pd.read_csv(csv_path)

titles = [
	"Temperature",
]

feature_keys = [
	"T (degC)",
]

colors = [
	"orange",
]

date_time_key = "Date Time"

# --------------------------- RAW DATA VISUALIZATION --------------------------

# Shows the weather data stored in the csv file on a plot.
# data - data parsed from a csv file containing weather data
#		*See above
def show_raw_visualization(data):
	time_data = data[date_time_key]
	fig, axes = plt.subplots(
		nrows=1, ncols=1, figsize=(15, 20), dpi=80, facecolor="w", edgecolor="k"
	)
	
	key = feature_keys[0]
	c = colors[0 % (len(colors))]
	t_data = data[key]
	t_data.index = time_data
	t_data.head()
	ax = t_data.plot(
		ax=axes,
		color=c,
		title="{} - {}".format(titles[0], key),
		rot=25,
	)
	ax.legend([titles[0]])

	plt.tight_layout()
	plt.show()
# end show_raw_visualization()

#show_raw_visualization(df)

# --------------------------- DATA PREPROCESSING ------------------------------

split_fraction = 0.715
train_split = int(split_fraction * int(df.shape[0]))
step = 6

past = 720
future = 72
learning_rate = 0.001
batch_size = 256
epochs = 10

# Normalizes all the data to a range of [0, 1] by subtracting the mean and
# dividing by the standard deviation of each feature.
def normalize(data, train_split):
	data_mean = data[:train_split].mean(axis=0)
	data_std = data[:train_split].std(axis=0)
	return (data - data_mean) / data_std

selected_parameters = [0]
print(
	"The selected parameters are:",
	", ".join([titles[i] for i in selected_parameters]),
)
selected_features = [feature_keys[i] for i in selected_parameters]
features = df[selected_features]
features.index = df[date_time_key]
features.head()

features = normalize(features.values, train_split)
features = pd.DataFrame(features)
features.head()

train_data = features.loc[0 : train_split - 1]
val_data = features.loc[train_split:]

# -----------------------------------------------------------------------------
# --------------------------- TRAIN DATASET -----------------------------------
# -----------------------------------------------------------------------------

start = past + future
end = start + train_split

# --------------------------- TRAINING DATASET --------------------------------

x_train = train_data[[i for i in range(len(selected_parameters))]].values
y_train = features.iloc[start:end]

sequence_length = int(past / step)

dataset_train = keras.preprocessing.timeseries_dataset_from_array(
	x_train,
	y_train,
	sequence_length=sequence_length,
	sampling_rate=step,
	batch_size=batch_size,
)

print(dataset_train)

# --------------------------- VALIDATION DATASET ------------------------------

x_end = len(val_data) - past - future

label_start = train_split + past + future

x_val = val_data.iloc[:x_end][[i for i in range(len(selected_parameters))]].values
y_val = features.iloc[label_start:][[0]]

dataset_val = keras.preprocessing.timeseries_dataset_from_array(
	x_val,
	y_val,
	sequence_length=sequence_length,
	sampling_rate=step,
	batch_size=batch_size,
)

for batch in dataset_train.take(1):
	inputs, targets = batch

print("Input shape:", inputs.numpy().shape)
print("Target shape:", targets.numpy().shape)

# --------------------------- TRAINING ----------------------------------------

# --- Create model ---
inputs = keras.layers.Input(shape=(inputs.shape[1], inputs.shape[2]))
lstm_out = keras.layers.LSTM(32)(inputs)
outputs = keras.layers.Dense(1)(lstm_out)

model = keras.Model(inputs=inputs, outputs=outputs)
model.compile(optimizer=keras.optimizers.Adam(learning_rate=learning_rate), loss="mse")
model.summary()

# --- Set up checkpoints ---

path_checkpoint = "model_checkpoint.h5"
es_callback = keras.callbacks.EarlyStopping(monitor="val_loss", min_delta=0, patience=5)

modelckpt_callback = keras.callbacks.ModelCheckpoint(
	monitor="val_loss",
	filepath=path_checkpoint,
	verbose=1,
	save_weights_only=True,
	save_best_only=True,
)

# --- Train Model ---
#epochs = 1	# REMOVE THIS <--
history = model.fit(
	dataset_train,
	epochs=epochs,
	validation_data=dataset_val,
	callbacks=[es_callback, modelckpt_callback],
)

def visualize_loss(history, title):
	loss = history.history["loss"]
	val_loss = history.history["val_loss"]
	epochs = range(len(loss))
	plt.figure()
	plt.plot(epochs, loss, "b", label="Training loss")
	plt.plot(epochs, val_loss, "r", label="Validation loss")
	plt.title(title)
	plt.xlabel("Epochs")
	plt.ylabel("Loss")
	plt.legend()
	plt.show()

visualize_loss(history, "Training and Validation Loss")

def show_plot(plot_data, delta, title):
	labels = ["History", "True Future", "Model Predictions"]
	marker = [".-", "rx", "go"]
	time_steps = list(range(-(plot_data[0].shape[0]), 0))
	if delta:
		future = delta
	else:
		future = 0

	plt.title(title)
	for i, val in enumerate(plot_data):
		if i:
			plt.plot(future, plot_data[i], marker[i], markersize=10, label=labels[i])
		else:
			plt.plot(time_steps, plot_data[i].flatten(), marker[i], label=labels[i])	
		plt.legend()
		plt.xlim([time_steps[0], (future + 5) * 2])
		plt.xlabel("Time-Step")
		plt.show()
		return 
	
for x, y in dataset_val.take(5):
	#print("x = ", x)
	#print("y = ", y)

	var1 = x[:, 1].numpy()
	var2 = y[0].numpy()
	var3 = model.predict(x)[0]

	#print(var1)
	#print(var2)
	print(var3)

	show_plot(
		[var1, var2, var3],
		12,
		"Single Step Prediction",
	)
